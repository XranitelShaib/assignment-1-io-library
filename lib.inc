section .data
	STDOUT equ 1          ; File descriptor for standard output
    STDIN equ 0           ; File descriptor for standard input
    SYS_EXIT equ 60       ; System call number for exit
    SYS_READ equ 0        ; System call number for read
    SYS_WRITE equ 1       ; System call number for write
    SPACE_CHAR equ 0x20   ; ASCII code for space character
    TAB_CHAR equ 0x9      ; ASCII code for tab character
    NEWLINE_CHAR equ 0xA  ; ASCII code for newline character
    MINUS_CHAR equ '-'    ; ASCII code for minus character
    ZERO_CHAR equ '0'     ; ASCII code for zero character
    DECIMAL_BASE equ 10   ; Decimal base for number conversion
	


section .text 
 
; Принимает код возврата и завершает текущий процесс
exit:
	mov rdi, rax 
    	mov rax, SYS_EXIT
	syscall
     

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
	.loop:
	cmp byte [rdi], 0
	je .end
	inc rax
	inc rdi
	jmp .loop
	.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rdi, STDOUT
	mov rax, SYS_WRITE
	syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    mov rax, SYS_WRITE
	mov rdx, 1
	push rdi 
	mov rsi, rsp
	mov rdi, STDOUT
	syscall
	pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, NEWLINE_CHAR
	call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax, rdi
	push rbx
	mov rbx, 10
	xor rcx, rcx
.loop:
	xor rdx, rdx
	div rbx
	add rdx, ZERO_CHAR
	push rdx
	inc rcx
	test rax, rax
	jnz .loop
.write_l:
	pop rdi
	push rcx
	call print_char
	pop rcx
	dec rcx
	test rcx, rcx
	jnz .write_l
	pop rbx
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
	jns print_uint
	push rdi
	mov rdi, MINUS_CHAR
	call print_char
	pop rdi
	neg rdi
	jmp print_uint
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rcx, rcx
	xor rax, rax
.while:
	mov al, byte[rdi + rcx]
	mov ah, byte[rsi + rcx]
	cmp al, ah
	jne .not_equals
	test al, al
	je .equals
	inc rcx
	jmp .while
.equals:
	mov rax, 1
	ret
.not_equals:
	xor rax, rax
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	sub rsp, 8
	mov rsi, rsp
	xor rdi, rdi
	xor rax, rax
	mov rdx, 1
	syscall
	test rax, rax
	jle .ex
	pop rax
	ret
.ex:
	pop rax
	xor rax, rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r12
	push r13
	push r14
	push rdi
	mov r12, rdi
	mov r13, rsi
	mov r14, rcx

.while_space:
	call read_char
	cmp rax, SPACE_CHAR
	je .while_space
	cmp rax, NEWLINE_CHAR
	je .while_space
	cmp rax, TAB_CHAR
	je .while_space

	xor rcx, rcx

.loop:
	inc r14
	cmp r14, r13
	jg .overflow
	mov byte[r12], al
	test rax, rax
	jz .end
	cmp rax, SPACE_CHAR
	je .end
	cmp rax, NEWLINE_CHAR
	je .end
	cmp rax, TAB_CHAR
	je .end
	inc r12
	call read_char

	jmp .loop

.overflow:
	mov rcx, r14
	dec rcx
	pop rdi
	xor rax, rax
	jmp .final

.end:
	mov rdx, r14
	dec rdx
	pop rax

.final:
	pop r14
	pop r13
	pop r12
	ret


 




; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	push rbx 
	xor rdx, rdx
	xor rax, rax
.loop:
	mov bl, byte[rdi+rdx]
	test bl, bl
	jz .end
	cmp bl, ZERO_CHAR
	jl .err
	cmp bl, '9'
	jg .err
	sub rbx, ZERO_CHAR
	imul rax, 10
	add rax, rbx
	inc rdx
	jmp .loop
.err:
	cmp rax, 0
	jnz .end
	xor rdx, rdx
.end:
	pop rbx
	ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	cmp byte[rdi], MINUS_CHAR
	jne parse_uint
	inc rdi
	call parse_uint
	test rdx, rdx
	jz .end
	inc rdx
	neg rax
	.end:
	ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
	push rsi
	push rdx
	call string_length
	xor rcx, rcx
	pop rdx
	pop rsi
	pop rdi
	inc rax
	cmp rax, rdx
	jg .err
.loop:
	cmp rax, rcx
	je .end
	mov dl, byte [rdi + rcx]
	mov [rsi + rcx], dl
	inc rcx
	jmp .loop	
.end:
	ret    
.err: 
	xor rax, rax
	ret

